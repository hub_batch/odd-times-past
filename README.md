# README #

# Odd Times Past #

## A story about escaping home.##

By Al Sturley

Navigate to the "downloads" section and download the .EXE file, then run it. If you need help, type the "help" command.

[If you don't want spoilers, stop reading now]

5/12/17

My favorite chapter in Odd Thomas was where Odd spoke to his mother; and subsequently revealed the terror he experienced as a child. I felt it gave a lot of insight on who Odd is in present day; which is why I wanted to do this. Odd mentions that he left home at sixteen, and subsequently found the people he's happy to call his friends/family. 

There's two endings just because it was a choose your own adventure game, and I couldn't really decide how I wanted the whole game to go. The first ending was there solely because of debug purposes, as I wanted to make sure the conditions for actually leaving would work. I decided to keep it in just for the heck of it.

The ending I'd consider the true one is the second, where Odd accidentally wakes his mother and all hell breaks loose. I feel like that's more accurate to his mother's character, and perhaps even Odd when he was young. 

[Technical information below]

The game runs on a get-input loop, that waits for input then sends it to another method to process it. In game development, I've learned it's best to have a "separation of concerns" as it's called. So you don't want the method that handles actually getting user input also processing it- that gives it too much to do. Process input is basically a long switch statement filled with whatever user input is valid, and subsequently where to go from there (usually another method, such as when the user types "look", it goes to the LookCommand method). 

All of the "actors" (characters) in the game are made using an Actor constructor, except for Odd. Odd needed an inventory list, so he has a separate object name, called Player. I'm sure there's a more elegant way of doing this, but at this point it works, so I'm not gonna change it. (However, if I do another project like this, I'll probably look into better ways of doing this.)

One thing I'm proud of is the AddToInventory and RemoveFromInventory methods. Originally, my bad programming habits dictated that I should check if an object was in the inventory within the *TakeCommand,* not the Add/RemoveFromInventory commands. Foolish. So when Add/RemoveFromInventory is called, it checks if the item is already in the user's inventory. Nifty. 

I've made a lot of text-adventure games, but really I've never finished them or gotten to a working state. This is the first. Yippee!