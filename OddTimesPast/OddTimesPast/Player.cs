﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddTimesPast
{
    class Player
    {
        public static List<Item> Inventory = new List<Item>();
        public int pHealth { get; set; }
        public string pName { get; set; }
        public string pLocation { get; set; }
        public Player(int PlayerHealth, string PlayerName, string PlayerLocation)
        {
            pHealth = PlayerHealth;
            pName = PlayerName;
            pLocation = PlayerLocation;
        }
        public static void AddToInventory(Item item)
        {
            if (Player.Inventory.Contains(item))
            {
                Console.WriteLine("You already have this item.");
            }
            else
            {
                Console.WriteLine("You have added the " + item.Name + " to your inventory.");
                Inventory.Add(item);
            }
        }
        public static void RemoveFromInventory(Item item)
        {
            if (!Player.Inventory.Contains(item))
            {
                Console.WriteLine("Error: Item is not in inventory");
            }
            else
            {
                Console.WriteLine("The " + item.Name + " has been removed from your inventory.");
                Inventory.Remove(item);
            }
        }
    }
}
