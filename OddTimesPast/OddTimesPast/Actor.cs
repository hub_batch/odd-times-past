﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddTimesPast
{
    class Actor
    {
        public int Health { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public bool IsTalkingOne { get; set; }
        public bool IsTalkingTwo { get; set; }

        public Actor(int ActorHealth, string ActorName, string ActorLocation, bool ActorTalkingOne, bool ActorTalkingTwo)
        {
            Health = ActorHealth;
            Name = ActorName;
            Location = ActorLocation;
            IsTalkingOne = ActorTalkingOne;
            IsTalkingTwo = ActorTalkingTwo;
        }

    }
}
