﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddTimesPast
{
    class Item
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public Item(int ItemID, string ItemName, string ItemLocation, string ItemDesc)
        {
            Name = ItemName;
            ID = ItemID;
            Location = ItemLocation;
            Description = ItemDesc;
        }
    }
}
