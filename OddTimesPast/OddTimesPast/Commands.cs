﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddTimesPast
{
    class Commands : Program
    {
        public static void MoveCommand()
        {
            if (response == "move")
            {
                Console.WriteLine("Where do you want to go?");
                Console.WriteLine("Command List: ");
                switch (Odd.pLocation)
                {
                    case "My Room":
                        Console.WriteLine("Hallway");
                        break;
                    case "Hallway":
                        Console.WriteLine("Mom's Room, Living Room, My Room");
                        break;
                    case "Living Room":
                        Console.WriteLine("Hallway, Breezeway");
                        break;
                    case "Mom's Room":
                        Console.WriteLine("Hallway");
                        break;
                    case "Breezeway":
                        Console.WriteLine("Front Door, Living Room");
                        break;
                    case "Front Door":
                        Console.WriteLine("Breezeway");
                        break;
                }
                GetInput();
            }
            switch (response)
            {
                case "hallway":
                    if (Odd.pLocation == Location[0] || Odd.pLocation == Location[2] || Odd.pLocation == Location[3])
                    {
                        Odd.pLocation = Location[1];
                        Console.WriteLine("You creep slowly into the hallway, being sure to stay near the walls.");
                        Console.WriteLine("The boards have settled there. You know that by walking here, you will make as little noise as possible.");
                        Console.WriteLine("Your mother's room is to the left. Forward is the living room.");
                        GetInput();
                    }
                    else
                    {
                        Console.WriteLine("Where are you going?");
                        GetInput();
                    }
                    break;
                case "living room":
                    if (Odd.pLocation == Location[1] || Odd.pLocation == Location[4])
                    {
                        Odd.pLocation = Location[2];
                        Console.WriteLine("You slip into the living room.");
                        GetInput();
                    }
                    else
                    {
                        Console.WriteLine("Where are you going?");
                        GetInput();
                    }
                    break;
                case "my room":
                    if (Odd.pLocation == Location[1])
                    {
                        Odd.pLocation = Location[0];
                        Console.WriteLine("You go into your room.");
                        GetInput();
                    }
                    else
                    {
                        Console.WriteLine("Where are you going?");
                        GetInput();
                    }
                    break;
                case "mom's room":
                    if (Odd.pLocation == Location[1])
                    {
                        if (Player.Inventory.Contains(BrassKey))
                        {
                            Console.WriteLine("You use the key, turning the lock with the care and force of a mouse.");
                            Console.WriteLine("Staying close to the walls, even going so far as to press yourself against them, you slip into your mother's room.");
                            Console.WriteLine("She is sleeping.");
                            Player.RemoveFromInventory(BrassKey);
                            Odd.pLocation = Location[3];
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("It's locked.");
                            GetInput();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Where are you going?");
                        GetInput();
                    }
                    break;
                case "breezeway":
                    if (Odd.pLocation == Location[2] || Odd.pLocation == Location[5])
                    {
                        Odd.pLocation = Location[4];
                        Console.WriteLine("You step into the breezeway.");
                        GetInput();
                    }
                    else
                    {
                        Console.WriteLine("Where are you going?");
                        GetInput();
                    }
                    break;
                case "front door":
                    if (Odd.pLocation == Location[4])
                    {
                        Odd.pLocation = Location[5];
                        if (Player.Inventory.Contains(HouseKey) && Player.Inventory.Contains(FlashLight) && Player.Inventory.Contains(Bag) && Player.Inventory.Contains(Clothes))
                        {
                            Console.WriteLine("You unlock the door, still being careful to not make a single noise. You don't want to wake anyone in the house. ");
                            Console.WriteLine("They don't need to know when, or where you went.");
                            Console.WriteLine("Ending No. 1: Peaceful exit.");
                            Console.ReadKey();
                            Environment.Exit(0);
                        }
                        else
                        {
                            Console.WriteLine("You don't have everything you need. You have to be prepared.");
                            GetInput();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Where are you going?");
                        GetInput();
                    }
                    break;
            }

        }
        public static void TakeCommand()
        {
            if (response.Contains("take"))
            {
                string item = response.Substring(4).Trim().ToLower();

                if (string.IsNullOrEmpty(item))
                {
                    Console.WriteLine("What are you doing?");
                    GetInput();
                }
                else
                {
                    if (item.Contains("brass key"))
                    {
                        if (Player.Inventory.Contains(BrassKey))
                        {
                            Console.WriteLine("You already have this item.");
                            GetInput();
                        }
                        else
                        {
                            if (Odd.pLocation == Location[4])
                            {
                                Player.AddToInventory(BrassKey);
                                GetInput();
                            }
                            else
                            {
                                Console.WriteLine("That item isn't here.");
                                GetInput();
                            }
                        }

                    }

                    if (item.Contains("flash light") || item.Contains("flashlight"))
                    {
                        if (Odd.pLocation == Location[0])
                        {
                            Player.AddToInventory(FlashLight);
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("That item isn't here.");
                            GetInput();
                        }
                    }
                    if (item.Contains("house key"))
                    {
                        if (Odd.pLocation == Location[4])
                        {
                            Player.AddToInventory(HouseKey);
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("That item isn't here.");
                            GetInput();
                        }
                    }
                    if (item.Contains("bag"))
                    {
                        if (Odd.pLocation == Location[0])
                        {
                            Player.AddToInventory(Bag);
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("That item isn't here.");
                            GetInput();
                        }

                    }
                    if (item.Contains("clothes"))
                    {
                        if (Odd.pLocation == Location[0])
                        {
                            if (Player.Inventory.Contains(Bag))
                            {
                                Player.AddToInventory(Clothes);
                                GetInput();
                            }
                            else
                            {
                                Console.WriteLine("You have nothing to put your clothes in. Better grab a bag first.");
                                GetInput();
                            }
                        }
                        else
                        {
                            Console.WriteLine("That item isn't here.");
                            GetInput();
                        }
                    }
                    if (item.Contains("money"))
                    {
                        if (Odd.pLocation == Location[3])
                        {
                            Console.WriteLine("You hold your breath, hand hovering a moment too long over the money you so desperately need-");
                            Console.WriteLine("[" + Mother.Name + "]:" + "What are you doing?");
                            Console.WriteLine("Commands: hello");
                            Mother.IsTalkingOne = true;
                            TalkCommand(talkresponse);
                        }
                        else
                        {
                            Console.WriteLine("That item isn't here.");
                            GetInput();
                        }
                    }
                }
            }
        }
        public static void LookCommand()
        {
            if (response == "look")
            {
                switch (Odd.pLocation)
                {
                    case "My Room":
                        Console.WriteLine("Not much to call your own in here. You've been hoarding what you can get for as long as you can remember, and even that is very little. The more possessions you have, the more PROBLEMS happen.");
                        Console.WriteLine("You've always had just the bare minimum. Nothing more, nothing less. You learned long ago that asking for possessions is innapropriate. You have grown to like things very simple.");
                        Console.WriteLine("You store what you have under the bed. She never looks there.");
                        GetInput();
                        break;
                    case "Hallway":
                        Console.WriteLine("It's a simple hallway. You learned long ago, when she's locked away, walking near the walls(but nowhere near her door) is the best course to take. Walls are sturdy. They've settled the wood below, and therefore make very little noise. You wouldn't want to wake her.");
                        Console.WriteLine("Walking the center is dangerous. You press against the wall.");
                        Console.WriteLine("She's resting in her room, you note, looking at her locked door.");
                        Console.WriteLine("Best to stay silent. Don't wake her.");
                        GetInput();
                        break;
                    case "Living Room":
                        Console.WriteLine("It's a simple living room. You don't spend much time here. No one really does. It's a very sad situation.");
                        Console.WriteLine("You know the spare house key is under the rug.");
                        GetInput();
                        break;
                    case "Mom's Room":
                        Console.WriteLine("Don't dawdle. Get what you need and leave.");
                        GetInput();
                        break;
                    case "Breezeway":
                        Console.WriteLine("Just a simple breezeway. You can feel warm Pico Mundo air slipping in through the cracks of the front door. ");
                        Console.WriteLine("Almost there.");
                        GetInput();
                        break;
                    case "Front Door":
                        if (Player.Inventory.Contains(HouseKey))
                        {
                            Console.WriteLine("You have the key to the door. However, you need to make sure you have everything you need.");
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("You need to find the key.");
                            GetInput();
                        }
                        break;
                }
            }
        }
        public static void LookAtCommand()
        {
            if (response.Contains("look at"))
            {
                string lookat = response.Substring(7).Trim().ToLower();

                if (string.IsNullOrEmpty(lookat))
                {
                    Console.WriteLine("What are you looking at?");
                    GetInput();
                }
                else
                {
                    if (lookat.Contains("dresser"))
                    {
                        if (Odd.pLocation == Location[0])
                        {
                            Console.WriteLine("You have a few clothes stored away in the dresser.");
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("What are you looking at?");
                            GetInput();
                        }
                    }
                    if (lookat.Contains("bed"))
                    {
                        if (Odd.pLocation == Location[0])
                        {
                            Console.WriteLine("You've stored a flashlight, and your backpack under the bed, tucked away against the wall. The dark of the underside keeps it from view, but you know it's there.");
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("What are you looking at?");
                            GetInput();
                        }
                    }
                    if (lookat.Contains("rug"))
                    {
                        if (Odd.pLocation == Location[4])
                        {
                            Console.WriteLine("You lift the rug to find the housekey, right where you expect it. Except. .There's two keys. One is brass, the texture worn from a violent turn or several.");
                            Console.WriteLine("You know where this key goes.");
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("What are you looking at?");
                            GetInput();
                        }
                    }
                    if (lookat.Contains("nightstand"))
                    {
                        if (Odd.pLocation == Location[3])
                        {
                            Console.WriteLine("A roll of money sits on HER nightstand.");
                            Console.WriteLine("She's sleeping beside it.");
                            Console.WriteLine("Silence.");
                            GetInput();
                        }
                        else
                        {
                            Console.WriteLine("What are you looking at?");
                            GetInput();
                        }
                    }
                }
            }
        }
        public static void CheckCommand()
        {
            if (!Player.Inventory.Any())
            {
                Console.WriteLine("You have no items.");
            }
            else
            {
                Console.WriteLine("---Held Items---");
                Player.Inventory.ForEach(item => Console.WriteLine(item.Name + " - " + item.Description));
            }
            GetInput();
        }
        public static void TalkCommand(string talkresponse)
        {
            if (Mother.IsTalkingOne == true)
            {
                if (string.IsNullOrEmpty(talkresponse))
                {
                    Console.WriteLine("Say something!");
                    TalkInput();
                }
                else
                {
                    if (talkresponse.Contains("hello") || talkresponse.Contains("hi"))
                    {
                        Console.WriteLine("[" + Mother.Name + "]:" + "What are you DOING in here?");
                        Console.WriteLine("She sounds angry.");
                        Console.WriteLine("Commands: Nothing, Going away/leaving");
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                    if (talkresponse.Contains("leaving") || talkresponse.Contains("going away"))
                    {
                        Console.WriteLine("She sits up, looking almost possessed. Her face is illuminated by the moon, casting a milky heat to permeate her features.");
                        Console.WriteLine("You can't read her like you read the dead. When they don't speak, they speak with their hands, their emotions.");
                        Console.WriteLine("Your mother is erratic and unpredictable.");
                        Console.WriteLine("The only predictable aspect of your life is the bullet in the chamber.");
                        Console.WriteLine("[" + Mother.Name + "]:" + "Get it.");
                        Console.WriteLine("You know exactly what she means.");
                        Console.WriteLine("Commands: Yes, No");
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                    if (talkresponse.Contains("yes") || talkresponse.Contains("okay"))
                    {
                        Console.WriteLine("Crouching down, from under her bed you pull out a case. It's heavy, but the weight is familiar in your hands.");
                        Console.WriteLine("You are sickened by the comfort you take in its weight.");
                        Console.WriteLine("You know exactly what she plans to do with the contents of the box.");
                        Console.WriteLine("You hand her the box, but before she can wrench it open and find comfort in cold steel, you bolt out of the room and into your own.");
                        Odd.pLocation = Location[0];
                        Console.WriteLine("You need to call someone. It's time, it's time.");
                        Console.WriteLine("You find your cell phone.");
                        Console.WriteLine("[???]: Hello, this is Chief Porter of the PMPD-");
                        Console.WriteLine("Commands: help");
                        Mother.IsTalkingOne = false;
                        Chief.IsTalkingOne = true;
                        TalkInput();

                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                    if (talkresponse.Contains("no") || talkresponse.Contains("nope"))
                    {
                        Console.WriteLine("As she throws the covers away, aiming to get up and do it herself, you stumble back. This is your only chance.");
                        Console.WriteLine("You turn and bolt from the room, running back into your own room.");
                        Odd.pLocation = Location[0];
                        Console.WriteLine("You need to call someone. It's time, it's time.");
                        Console.WriteLine("You find your cell phone.");
                        Console.WriteLine("[???]: Hello, this is Chief Porter of the PMPD-");
                        Console.WriteLine("Commands: help");
                        Mother.IsTalkingOne = false;
                        Chief.IsTalkingOne = true;
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                    if (talkresponse.Contains("nothing") || talkresponse.Contains("none of your buisness"))
                    {
                        Console.WriteLine("She throws her covers aside, causing you to stumble back. Turning on heel and running out of the room, you all but launch yourself into your room. The door slams behind you.");
                        Odd.pLocation = Location[0];
                        Console.WriteLine("You need to call someone. It's time.");
                        Console.WriteLine("You find your cell phone.");
                        Console.WriteLine("[???]: Hello, this is Chief Porter of the PMPD-");
                        Console.WriteLine("Commands: help");
                        Mother.IsTalkingOne = false;
                        Chief.IsTalkingOne = true;
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                }
            }
            if (Chief.IsTalkingOne == true)
            {
                if (string.IsNullOrEmpty(talkresponse))
                {
                    Console.WriteLine("Say something!");
                    TalkInput();
                }
                else
                {
                    if (talkresponse.Contains("help"))
                    {
                        Console.WriteLine("[" + Chief.Name + "]: " + "What's your name, son? Where are you?");
                        Console.WriteLine("You sputter out your name, breath coming in choked invervals. You're terrified. Either way, a car will come to your house.");
                        Console.WriteLine("You know this much. Calling the police for no good reason is a mistake. Even if your good reason is currently being held captive within your throat.");
                        Console.WriteLine("Commands: home");
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                    if (talkresponse.Contains("home") || talkresponse.Contains("my house"))
                    {
                        Console.WriteLine("You know how the police works. You learned about it not long ago.");
                        Console.WriteLine("They can tell where a call is comming from. An address escapes you, in your fever.");
                        Console.WriteLine("You can hear her coming.");
                        Console.WriteLine("[" + Chief.Name + "]: Odd? Odd, stay on the line with me, alright? We'll send someone right over, Odd.");
                        Console.WriteLine("His voice is calm. You wonder how someone could possibly be so calm in this situation. In this hell, in this place you've always been in.");
                        Console.WriteLine("The next few hours were a blur. You care not to remember nor recount them.");
                        Console.WriteLine("The police chief wants to talk to you. ");
                        Console.WriteLine("[" + Chief.Name + "]: " + "Can you tell me what happened here?");
                        Console.WriteLine("Commands: Abuse, Nothing");
                        Chief.IsTalkingOne = false;
                        Chief.IsTalkingTwo = true;
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                }

            }
            if (Chief.IsTalkingTwo == true)
            {
                if (string.IsNullOrEmpty(talkresponse))
                {
                    Console.WriteLine("Say something!");
                    TalkInput();
                }
                else
                {
                    if (talkresponse.Contains("abuse") || talkresponse.Contains("abused"))
                    {
                        Console.WriteLine("He looks troubled by these words. You feel guilt tear at your throat, almsot making you want to recoil. You will not speak any longer. You refuse.");
                        Console.WriteLine("[" + Chief.Name + "]: " + "You don't have to stay here anymore. I know someone who can give you a place. Do you want that, Odd?");
                        Console.WriteLine("Slowly, you nod.");
                        Console.WriteLine("Ending no. 2: Pull the trigger for me.");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                    if (talkresponse.Contains("nothing"))
                    {
                        Console.WriteLine("You can tell he doesn't believe you.");
                        Console.WriteLine("You've got to tell him what's happening.");
                        Console.WriteLine("Commands: Abuse, Nothing");
                        TalkInput();
                    }
                    else
                    {
                        Console.WriteLine("Try different input.");
                        TalkInput();
                    }
                }
            }
        }
        public static void HelpCommand()
        {
            Console.Clear();
            Console.WriteLine("---Commands---");
            Console.WriteLine("Move: Lists places you can go.");
            Console.WriteLine("Look: Gives a description of the room you're in.");
            Console.WriteLine("Look at [item]: Gives a description of a specific item in a room.");
            Console.WriteLine("Take [item]: Adds the given item to your inventory.");
            Console.WriteLine("Help: Displays this help screen.");
            Console.WriteLine("---Required Items---");
            Console.WriteLine("1x House Key");
            Console.WriteLine("1x Flashlight");
            Console.WriteLine("1x Bag");
            Console.WriteLine("1x Clothes");
            Console.WriteLine("---Optional Items---");
            Console.WriteLine("1x Money");
            Console.WriteLine("1x Brass Key");
            GetInput();

        }
    }
}



