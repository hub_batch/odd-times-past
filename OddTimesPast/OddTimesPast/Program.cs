﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddTimesPast
{
    class Program
    {
        public static string response;
        public static string talkresponse;
        public static bool test;
        public static Item BrassKey;
        public static Item FlashLight;
        public static Item HouseKey;
        public static Item Bag;
        public static Item Clothes;
        public static Item Money;
        public static Actor Mother;
        public static Actor Chief;
        public static Player Odd;
        public static List<string> Location = new List<string>
        {
            "My Room",
            "Hallway",
            "Living Room",
            "Mom's Room",
            "Breezeway",
            "Front Door",
        };
        static void Main(string[] args)
        {
            Game();
        }

        public static void Game()
        {
            Populate();
            Console.WriteLine("Your name is " + Odd.pName + ".");
            Console.WriteLine("You are sixteen years old. You are leaving your house.");
            Console.WriteLine("Better get ready.");
            GetInput();
        }
        public static void Populate()
        {
            BrassKey = new Item(0, "Brass Key", Location[2], "A simple brass key, worn with violent turns and tosses.");
            FlashLight = new Item(1, "Flashlight", Location[0], "A flashlight. It's one of those ones you shake to use.");
            HouseKey = new Item(2, "House Key", Location[2], "The key to your house. The spare one, of course. You were never given your own key.");
            Bag = new Item(3, "Bag", Location[0], "Just a simple bag.");
            Clothes = new Item(4, "Clothes", Location[0], "Very simple clothes. Two shirts, two pairs of pants.");
            Money = new Item(5, "Money", Location[3], "A roll of money. It's not yours.");
            Odd = new Player(20, "Odd", Location[0]);
            Mother = new Actor(10, "Mother", Location[3], false, false);
            Chief = new Actor(50, "Chief Porter", Location[5], false, false);
        }
        public static string GetInput()
        {
            test = true;
            while (test)
            {
                Console.Write(">");
                response = Console.ReadLine().ToLower();
                if (string.IsNullOrWhiteSpace(response))
                {
                    Console.WriteLine("I'm sorry, I don't understand.");
                    continue;
                }
                else
                {
                    test = false;
                    ProcessInput(response);
                }

            }
            return response;
        }
        public static void ProcessInput(string response)
        {
            switch (response)
            {
                case "move":
                    Commands.MoveCommand();
                    break;
                case "hallway":
                    Commands.MoveCommand();
                    break;
                case "living room":
                    Commands.MoveCommand();
                    break;
                case "mom's room":
                    Commands.MoveCommand();
                    break;
                case "breezeway":
                    Commands.MoveCommand();
                    break;
                case "front door":
                    Commands.MoveCommand();
                    break;
                case "my room":
                    Commands.MoveCommand();
                    break;
                case "look":
                    Commands.LookCommand();
                    break;
                case "look at":
                    Commands.LookAtCommand();
                    break;
                case "take":
                    Commands.TakeCommand();
                    break;
                case "check items":
                    Commands.CheckCommand();
                    break;
                case "check":
                    Commands.CheckCommand();
                    break;
                case "check inventory":
                    Commands.CheckCommand();
                    break;
                case "look at dresser":
                    Commands.LookAtCommand();
                    break;
                case "look at bed":
                    Commands.LookAtCommand();
                    break;
                case "look at nightstand":
                    Commands.LookAtCommand();
                    break;
                case "look at rug":
                    Commands.LookAtCommand();
                    break;
                case "take flashlight":
                    Commands.TakeCommand();
                    break;
                case "take brass key":
                    Commands.TakeCommand();
                    break;
                case "take house key":
                    Commands.TakeCommand();
                    break;
                case "take bag":
                    Commands.TakeCommand();
                    break;
                case "take clothes":
                    Commands.TakeCommand();
                    break;
                case "take money":
                    Commands.TakeCommand();
                    break;
                case "help":
                    Commands.HelpCommand();
                    break;
                default:
                    Console.WriteLine("I'm sorry, I don't understand.");
                    GetInput();
                    break;
            }
        }
        public static string TalkInput()
        {
            test = true;
            while (test)
            {
                Console.Write(">");
                talkresponse = Console.ReadLine().ToLower();
                if (string.IsNullOrWhiteSpace(response))
                {
                    continue;
                }
                else
                {
                    test = false;
                    Commands.TalkCommand(talkresponse);
                }
            }
            return talkresponse;
        }
    }
}

